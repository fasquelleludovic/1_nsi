# Vrai ou faux


On interprète en général 0 par Faux et 1 par Vrai.


## Table du et

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-uys7">x<br></th>
    <th class="tg-uys7">y<br></th>
    <th class="tg-c3ow">x et y<br></th>
  </tr>
  <tr>
    <td class="tg-uys7">faux</td>
    <td class="tg-uys7">faux</td>
    <td class="tg-c3ow">faux</td>
  </tr>
  <tr>
    <td class="tg-uys7">faux</td>
    <td class="tg-uys7">vrai</td>
    <td class="tg-c3ow">faux</td>
  </tr>
  <tr>
    <td class="tg-c3ow">vrai</td>
    <td class="tg-c3ow">faux</td>
    <td class="tg-c3ow">faux</td>
  </tr>
  <tr>
    <td class="tg-c3ow">vrai</td>
    <td class="tg-c3ow">vrai</td>
    <td class="tg-c3ow">vrai</td>
  </tr>
</table>



## Table du ou

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-uys7">x<br></th>
    <th class="tg-uys7">y<br></th>
    <th class="tg-c3ow">x ou y<br></th>
  </tr>
  <tr>
    <td class="tg-uys7">faux</td>
    <td class="tg-uys7">faux</td>
    <td class="tg-c3ow">faux</td>
  </tr>
  <tr>
    <td class="tg-uys7">faux</td>
    <td class="tg-uys7">vrai</td>
    <td class="tg-c3ow">vrai<br></td>
  </tr>
  <tr>
    <td class="tg-c3ow">vrai</td>
    <td class="tg-c3ow">faux</td>
    <td class="tg-c3ow">vrai</td>
  </tr>
  <tr>
    <td class="tg-c3ow">vrai</td>
    <td class="tg-c3ow">vrai</td>
    <td class="tg-c3ow">vrai</td>
  </tr>
</table>



## table du ou exclusif

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-uys7">x<br></th>
    <th class="tg-uys7">y<br></th>
    <th class="tg-c3ow">x xor y<br></th>
  </tr>
  <tr>
    <td class="tg-uys7">faux</td>
    <td class="tg-uys7">faux</td>
    <td class="tg-c3ow">faux</td>
  </tr>
  <tr>
    <td class="tg-uys7">faux</td>
    <td class="tg-uys7">vrai</td>
    <td class="tg-c3ow">vrai<br></td>
  </tr>
  <tr>
    <td class="tg-c3ow">vrai</td>
    <td class="tg-c3ow">faux</td>
    <td class="tg-c3ow">vrai</td>
  </tr>
  <tr>
    <td class="tg-c3ow">vrai</td>
    <td class="tg-c3ow">vrai</td>
    <td class="tg-c3ow">faux</td>
  </tr>
</table>



## table du non

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-uys7">x<br></th>
    <th class="tg-uys7">non(x)<br></th>
  </tr>
  <tr>
    <td class="tg-uys7">faux</td>
    <td class="tg-uys7">vrai</td>
  </tr>
  <tr>
    <td class="tg-uys7">vrai</td>
    <td class="tg-uys7">faux</td>
  </tr>
</table>
