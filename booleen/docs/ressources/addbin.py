
def binaire(n):
    """
    n -- entier naturel
    
    renvoi -- liste des bits de l'écriture binaire de n, chiffres des unités à gauche
    """
    if n == 0:  return [0]
    listeBits = []
    while n != 0:
        listeBits.append(n%2)
        n = n//2
    return listeBits
   
def egaliseLonguer(a, b):
    """
    a -- liste de bits
    b -- liste de bits
    
    effet -- ajoute des 0 à droite de la liste la plus courte
             pour qu'elle ait même longueur que l'autre.
    """
    if len(a) > len(b):
        for _ in range(len(b), len(a)):
            b.append(0)
    else:
        for _ in range(len(a), len(b)):
            a.append(0)
    
    
def et(a, b):
    """
    a -- 0 ou 1
    b -- 0 ou 1
    
    renvoie a et b
    """
    return a*b
    
def ou(a,b):
    return max(a,b)
    
def non(a):
    return 1-a
    
def additionBinaire(a,b):
    """
    a -- liste des bits de l'écriture binaire d'un entier, unité en indice 0
    b -- liste des bits de l'écriture binaire d'un entier, unité en indice 0
    
    renvoi -- liste des bits de la somme a + b
    """
    egaliseLonguer(a, b)
    resultat = [0 for i in range(len(a)+1)]
    
    retenue = False
    for i in range(len(a)):
        u = a[i]
        v = b[i]
        resultat[i] += (u and non(v) and non(retenue)) or (not u and v and not retenue) or (not u and not v and retenue) or (u and v and retenue)
        retenue = (u and v) or (v and retenue) or (u and retenue)
    resultat[len(a)] =  retenue 
    
    return resultat
    
    
m = additionBinaire( binaire(2), binaire(3)) 
print(m)
    
 
    
