# Et, Ou, Non



## ET

En informatique, la fonction 'et' est très souvent nommée par son nom anglais: 'and'.
 
### Définition

La fonction "et" est une fonction de $\mathbb{B}\times\mathbb{B}$ dans $\mathbb{B}$ définie par:

+ et(0; 0) = 0
+ et(0; 1) = 0
+ et(1; 0) = 0
+ et(1; 1) = 1

Ce que l'on présente en général sous forme d'un tableau:

 

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-uys7">a</th>
    <th class="tg-uys7">b</th>
    <th class="tg-uys7">et(a;b) <br>noté aussi "a et b"</th>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">1</td>
    <td class="tg-uys7">0</td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">0</td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
  </tr>
</table>

 
 
  

 



### En bref

On peut résumer l'opérateur 'et' par l'équivalence:

$$(a \text{ et } b = 1) \Longleftrightarrow (a,b)=(1,1)$$

Cette équivalence exprime que le seul cas donnant 1 est le cas "1 et 1". Tous les autres cas donnent 0.

 
 
 
 
 
 
 
 
 
 
 
 
## OU

La fonction 'ou' est très souvent nommée par son nom anglais: 'or'.

### La fonction 'ou'.

La fonction 'ou' est une fonction de $\mathbb{B}\times\mathbb{B}$ dans $\mathbb{B}$ définie par:

+ ou(0; 0) = 0
+ ou(0; 1) = 1
+ ou(1; 0) = 1
+ ou(1; 1) = 1

Ce que l'on présente en général sous forme d'un tableau:

 
<table class="tg">
  <tr>
    <th class="tg-uys7">a</th>
    <th class="tg-uys7">b</th>
    <th class="tg-uys7">ou(a;b) <br>noté aussi "a ou b"</th>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">1</td>
    <td class="tg-uys7">1<br></td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">1</td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
  </tr>
</table>

 
 

### En bref

On peut résumer l'opérateur 'ou' par l'équivalence:

$$(a \text{ ou } b = 0) \Longleftrightarrow (a, b) = (0, 0)$$

Cette équivalence exprime que le seul cas donnant 0 est le cas "0 ou 0". Tous les autres cas donnent 1.



## Non


En informatique, la fonction non est très souvent nommée par son nom anglais: 'not'.



La fonction 'non' est une fonction de $\mathbb{B}$ dans $\mathbb{B}$ définie par:

+ non(0) = 1
+ non(1) = 0
 

Ce que l'on présente en général sous forme d'un tableau:
 
<table class="tg">
  <tr>
    <th class="tg-s6z2">a</th>
    <th class="tg-s6z2">non(a) <br> noté aussi "non a"</th>
  </tr>
  <tr>
    <td class="tg-s6z2">0</td>
    <td class="tg-s6z2">1</td>
  </tr>
  <tr>
    <td class="tg-s6z2">1</td>
    <td class="tg-s6z2">0</td>
  </tr>
</table>


 

Il est également fréquent de noter non(a) sous la forme $\overline{a}$.

  


