# Exercices


## nand

Pour tous booléens a et b, on note non_et(a; b) = non(a et b).

En anglais, cette fonction est nommée *nand* (abréviation de not and).

+ Dresser la table du nand.




??? note solution "Table du nand"

    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-s6z2{text-align:center}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    </style>
    <table class="tg">
      <tr>
        <th class="tg-s6z2">a</th>
        <th class="tg-s6z2">b</th>
        <th class="tg-baqh">a et b</th>
        <th class="tg-baqh">non(a et b) = nand(a,b)</th>
      </tr>
      <tr>
        <td class="tg-s6z2">0</td>
        <td class="tg-s6z2">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-s6z2">0</td>
        <td class="tg-s6z2">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>
    
    
+ Exprimer non(a et b) avec un "ou".    
    
  

??? note solution "Expression équivalente"

    La table ci-dessous montre que non(a et b) = non(a) ou non(b)

   
    <table class="tg">
      <tr>
        <th class="tg-9wq8">a</th>
        <th class="tg-9wq8">b</th>
        <th class="tg-9wq8">non(a)</th>
        <th class="tg-9wq8">non(b)</th>
        <th class="tg-baqh">non(a) ou non(b)</th>
      </tr>
      <tr>
        <td class="tg-9wq8">0</td>
        <td class="tg-9wq8">0</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-9wq8">0</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">0</td>
        <td class="tg-9wq8">0</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">0<br></td>
        <td class="tg-9wq8">0</td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>
        
    
    
    
    
    
    
    
    
    
    
    
        
## nor


Pour tous booléens a et b, on note non_ou(a; b) = non(a ou b).

En anglais, la fonction est nommée *nor*.

+ Dresser la table du nor.


??? note solution "Table du nor"

   
    <table class="tg">
      <tr>
        <th class="tg-s6z2">a</th>
        <th class="tg-s6z2">b</th>
        <th class="tg-baqh">a ou b</th>
        <th class="tg-baqh">non(a ou b) = nor(a,b)</th>
      </tr>
      <tr>
        <td class="tg-s6z2">0</td>
        <td class="tg-s6z2">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-s6z2">0</td>
        <td class="tg-s6z2">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>
    
    
+ Exprimer nor avec un "et".    
    
    

??? note solution "Expression équivalente"

    La table ci-dessous montre que non(a ou b) = non(a) et non(b)
    
   
    <table class="tg">
      <tr>
        <th class="tg-9wq8">a</th>
        <th class="tg-9wq8">b</th>
        <th class="tg-9wq8">non(a)</th>
        <th class="tg-9wq8">non(b)</th>
        <th class="tg-baqh">non(a) et non(b)</th>
      </tr>
      <tr>
        <td class="tg-9wq8">0</td>
        <td class="tg-9wq8">0</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-9wq8">0</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">0</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">0</td>
        <td class="tg-9wq8">0</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">1</td>
        <td class="tg-9wq8">0<br></td>
        <td class="tg-9wq8">0</td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>    
        
    
    
    
    
    
    
## non( non() et non() )

Pour tous booléens a et b, on pose f(a, b) = non( non(a) et non(b)).

+ Dresser la table de f.
+ Comment peut-on écrire f plus simplement?



??? note solution "table"

    
    <table class="tg">
      <tr>
        <th class="tg-uys7">a</th>
        <th class="tg-uys7">b</th>
        <th class="tg-c3ow">non(a)</th>
        <th class="tg-c3ow">non(b)</th>
        <th class="tg-baqh">non(a) et non(b)</th>
        <th class="tg-baqh">non(non(a) et non(b))</th>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">0</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-baqh">1<br></td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-c3ow">0<br></td>
        <td class="tg-c3ow">1<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0<br></td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>


??? note solution "Expression simplifiée"

    Avec la table précédente, on constate que f = ou. Cela résulte également des exercices précédents:
    
    non( non(a) et non(b)) =  nand(non(a); non(b))    
    = non(non(a)) ou non(non(b)) = a ou b. 
    
    
    
    
## non( non() ou non() )

Pour tous booléens a et b, on pose g(a, b) = non( non(a) ou non(b)).

+ Dresser la table de g.
+ Comment peut-on écrire g plus simplement?



??? note solution "table"

    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    </style>
    <table class="tg">
      <tr>
        <th class="tg-uys7">a</th>
        <th class="tg-uys7">b</th>
        <th class="tg-c3ow">non(a)</th>
        <th class="tg-c3ow">non(b)</th>
        <th class="tg-baqh">non(a) ou non(b)</th>
        <th class="tg-baqh">non(non(a) et non(b))</th>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">0</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-baqh">1<br></td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">1<br></td>
        <td class="tg-baqh">0<br></td>
      </tr>
      <tr>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-c3ow">0<br></td>
        <td class="tg-c3ow">1<br></td>
        <td class="tg-baqh">1<br></td>
        <td class="tg-baqh">0<br></td>
      </tr>
      <tr>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0<br></td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>


??? note solution "Expression simplifiée"

    Avec la table précédente, on constate que g = et.  
    Cela résulte également des exercices précédents: non( non(a) ou non(b)) = non(non(a)) et non(non(b)) =
    a et b.
    














    
## (x et non(y) ) ou (non(x) et y)

Pour tous booléens x et y, on pose p(x, y) = (x et non(y) ) ou (non(x) et y).

+ Dresser la table de p.
+ Comment peut-on écrire p plus simplement?


??? note solution "table"

    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    .tg .tg-0lax{text-align:left;vertical-align:top}
    </style>
    <table class="tg">
      <tr>
        <th class="tg-uys7">x<br></th>
        <th class="tg-uys7">y<br></th>
        <th class="tg-c3ow">non(x)</th>
        <th class="tg-c3ow">non(y)</th>
        <th class="tg-baqh">x et non(y)</th>
        <th class="tg-baqh">non(x) et y <br></th>
        <th class="tg-0lax">(x et non(y)) ou (non(x) et y)</th>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">0</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0<br></td>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">1<br></td>
        <td class="tg-baqh">1<br></td>
      </tr>
      <tr>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-c3ow">0<br></td>
        <td class="tg-c3ow">1<br></td>
        <td class="tg-baqh">1<br></td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">1<br></td>
      </tr>
      <tr>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0<br></td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">0<br></td>
      </tr>
    </table>


??? note solution "Expression simplifiée"

    Avec la table précédente, on constate que p = xor.





















   
    
## ( non(x) et non(y) ) ou (x et y)

Pour tous booléens x et y, on pose h(x, y) = ( non(x) et non(y) ) ou (x et y).

Dresser la table de h.
 


??? note solution "table"

    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    .tg .tg-0lax{text-align:left;vertical-align:top}
    </style>
    <table class="tg">
      <tr>
        <th class="tg-uys7">x<br></th>
        <th class="tg-uys7">y<br></th>
        <th class="tg-c3ow">non(x)</th>
        <th class="tg-c3ow">non(y)</th>
        <th class="tg-baqh">non(x) et non(y)</th>
        <th class="tg-baqh">x et y <br></th>
        <th class="tg-0lax">(x et y) ou (non(x) et non(y))</th>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">0</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-baqh">1<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-c3ow">0<br></td>
        <td class="tg-c3ow">1<br></td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0<br></td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>


??? note solution "Expression simplifiée"

    Avec la table des valeurs de vérité, on constate que h(x, y) = non(x xor y).
    En anglais, on ne note pas en général NXOR cette fonction mais plutôt XNOR.
    
    h est la fonction ssi (équivalence): h(x,y)=1 $\Longleftrightarrow$ x=y.
