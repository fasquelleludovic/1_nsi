# QCM  


!!! note  
    Les QCM sont là pour vous aider à contrôler ce que vous avez retenu.
    Si vous ne répondez pas à toutes les questions sans hésitation, c'est sans doute
    qu'il faut refaire des lectures des pages précédentes.

Trouvez la ou les bonnes réponses.

## QCM 

Voici la table de vérité d'une fonction (nommée multiplexeur):

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-baqh{text-align:center;vertical-align:top}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-uys7">a<br></th>
    <th class="tg-uys7">b<br></th>
    <th class="tg-baqh">c</th>
    <th class="tg-baqh">mux(a,b,c)</th>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
    <td class="tg-baqh">0</td>
    <td class="tg-baqh">0</td>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">0</td>
  </tr>
  <tr>
    <td class="tg-baqh">0</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">0<br></td>
    <td class="tg-baqh">1</td>
  </tr>
  <tr>
    <td class="tg-baqh">0</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">1</td>
  </tr>
  <tr>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">0<br></td>
    <td class="tg-baqh">0</td>
    <td class="tg-baqh"><br>0</td>
  </tr>
  <tr>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">0</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">1</td>
  </tr>
  <tr>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">0</td>
    <td class="tg-baqh">0</td>
  </tr>
  <tr>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">1</td>
  </tr>
</table>


Laquelle des expressions lui correspond:

- [ ] (not(a) and b) or (a and c) 
- [ ] (a and b) or (not(a) and c) 
- [ ] (not(a) or b) and (a or c) 
- [ ] b or (a and c) 


??? note solution "Réponse"

    - [X] (not(a) and b) or (a and c) 
    - [ ] (a and b) or (not(a) and c) 
    - [ ] (not(a) or b) and (a or c) 
    - [ ] b or (a and c) 

## QCM 

Pour dresser une table de vérité, on utilise la valeur 1 pour True et 0 pour False.
Laquelle des fonctions suivantes ne correspond pas à une fonction "ou":

- [ ] `def f(a,b): return a+b-a*b`
- [ ] `def g(a,b): return max(a,b)`
- [ ] `def h(a,b): return 1 - (1-a)*(1-b)`
- [ ] `def k(a,b): return (a+b)%2`


??? note solution "Réponse"


    - [ ] `def f(a,b): return a+b-a*b`
    - [ ] `def g(a,b): return max(a,b)`
    - [ ] `def h(a,b): return 1 - (1-a)*(1-b)`
    - [X] `def k(a,b): return (a+b)%2`



## QCM 

La fonction suivante:

```python
def table():
    resultat = ""
    for a in (False, True):
        for b in (False, True):
            resultat += f"{(a or b) and a} "
    return resultat
```

renvoie:

- [ ] False False True True
- [ ] False True False True
- [ ] True False True False
- [ ] True True False False

??? note solution "Réponse"


    - [X] False False True True
    - [ ] False True False True
    - [ ] True False True False
    - [ ] True True False False
    
    
    Pour bien comprendre la réponse, dresser la table de vérité de l'expression (a ou b) et a:
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    </style>
    <table class="tg">
      <tr>
        <th class="tg-uys7">a<br></th>
        <th class="tg-uys7">b<br></th>
        <th class="tg-baqh">a or b</th>
        <th class="tg-baqh">(a or b) and a</th>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">1<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>    
    
    
 
## QCM

Pour deux booléens x et y, on définit x ↑ y par x ↑ y = non( x et y) 
(opération appelée [barre de Sheffer](https://fr.wikipedia.org/wiki/Barre_de_Sheffer)).

Une autre expression de (x ↑ y) ↑ (x ↑ y) est:

- [ ] x ou y
- [ ] x et y
- [ ] non(x et y)
- [ ] non(x ou y)


??? note solution "Réponse"

    - [ ] x ou y
    - [X] x et y
    - [ ] non(x et y)
    - [ ] non(x ou y)
    
    
    La table de vérité est en effet la suivante:
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    </style>
    <table class="tg">
      <tr>
        <th class="tg-uys7">a<br></th>
        <th class="tg-uys7">b<br></th>
        <th class="tg-baqh">a et b</th>
        <th class="tg-baqh">a ↑ b = non(a et b)</th>
        <th class="tg-baqh">(a ↑ b) et (a ↑ b)</th>
        <th class="tg-baqh">(a ↑ b) ↑ (a ↑ b) = non((a ↑ b) et (a ↑ b)) </th>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>
















## QCM 

L'expression (a ou b) and a est égale à:

- [ ] a ou b
- [ ] b
- [ ] a
- [ ] a xor b

??? note solution "Réponse"


    - [ ] a ou b
    - [ ] b
    - [X] a
    - [ ] a xor b
    
    Pour bien comprendre la réponse, dresser la table de vérité de l'expression (a ou b) et a:
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    </style>
    <table class="tg">
      <tr>
        <th class="tg-uys7">a<br></th>
        <th class="tg-uys7">b<br></th>
        <th class="tg-baqh">a or b</th>
        <th class="tg-baqh">(a or b) and a</th>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">1<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>   
    
    
    
    
    
    
    
    
    
    
    
    
    
    

## QCM 

Pour deux booléens x et y, on définit x → y par x → y = non(x) ou y 
(opération appelée [implication](https://fr.wikipedia.org/wiki/Implication_(logique))).

Une autre expression de   ((x → y) et x) → y  est:

- [ ] x et x
- [ ] x ou non(x)
- [ ] x et non(x)
- [ ] x


??? note solution "Réponse"

    - [ ] x et x
    - [X] x ou non(x)
    - [ ] x et non(x)
    - [ ] x et non(y)

    
    
    La table de vérité de  I(x,y) = ((x → y) et x) → y est en effet la suivante:
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    </style>
    <table class="tg">
      <tr style="background-color:#ccc;">
        <th class="tg-uys7">x<br></th>
        <th class="tg-uys7">y</th>
        <th class="tg-baqh">non(x)</th>
        <th class="tg-baqh">x→y&nbsp;&nbsp;=&nbsp;&nbsp;non(x) ou y</th>
        <th class="tg-baqh"> (x→y) et x</th>
        <th class="tg-baqh">non((x→y) et x)</th>
        <th class="tg-baqh">((x → y) et x) → y   =  non((x→y) et x)  ou y<br></th>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-uys7">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>



    et la table de f(x,y) = x ou non(x):
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    </style>
    <table class="tg">
      <tr style="background-color:#ccc;">
        <th class="tg-uys7">x<br></th>
        <th class="tg-baqh">y</th>
        <th class="tg-baqh">non(x)</th>
        <th class="tg-baqh">x ou non(x)<br></th>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0<br></td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>




