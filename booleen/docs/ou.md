# Ou

En informatique, la fonction 'ou' est très souvent nommée par son nom anglais: 'or'.

## La fonction 'ou'.

La fonction 'ou' est une fonction de $\mathbb{B}\times\mathbb{B}$ dans $\mathbb{B}$ définie par:

+ ou(0; 0) = 0
+ ou(0; 1) = 1
+ ou(1; 0) = 1
+ ou(1; 1) = 1

Ce que l'on présente en général sous forme d'un tableau:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-uys7">a</th>
    <th class="tg-uys7">b</th>
    <th class="tg-uys7">ou(a;b)</th>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">1</td>
    <td class="tg-uys7">1<br></td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">1</td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
  </tr>
</table>

## L'opérateur 'ou'

Au lieu de voir 'ou' comme une fonction et d'écrire ou(a; b), on utilise plutôt 'ou' comme un opérateur
et on écrit "a ou b".

La table du 'ou':

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-uys7">a</th>
    <th class="tg-uys7">b</th>
    <th class="tg-uys7">a ou b<br></th>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">1</td>
    <td class="tg-uys7">1<br></td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">1</td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
  </tr>
</table>



## En bref

On peut résumer l'opérateur 'ou' par l'équivalence:

$$(a \text{ ou } b = 0) \Longleftrightarrow (a, b) = (0, 0)$$

Cette équivalence exprime que le seul cas donnant 0 est le cas "0 ou 0". Tous les autres cas donnent 1.


## Une autre expression.

En interprétant 0 et 1 comme des entiers naturels, 
on peut également retenir la table du 'ou' en remarquant que l'on a:

$$a \text{ ou } b = a + b - a\times b$$

On pourra faire l'analogie avec les probabilités: 
$P\left( A \text{ ou } B\right) = P(A) + P(B) - P\left(A \text{ et } B\right)$.

Ou encore:

$$a \text{ ou } b = \max(a; b)$$

   
