# Addition binaire

Réaliser une addition de deux entiers en binaire est l'une des tâches   courantes dans les circuits d'une machine.
Nous voyons ci-dessous qu'une telle addition se traduit simplement par des fonctions booléennes, c'est ce qui permet de traduire
cela en machine par des circuits adpatés (nous y revenons plus tard dans le cours).

## Somme de deux bits

Compléter le tableau suivant (où les nombres sont écrits en base deux):

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-baqh{text-align:center;vertical-align:top}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr style="background-color:#ccc;">
    <th class="tg-uys7">x<br></th>
    <th class="tg-baqh">y</th>
    <th class="tg-baqh">x + y</th>
    <th class="tg-baqh">chiffre des "deuzaines"</th>
    <th class="tg-baqh">chiffre des unités</th>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-baqh">0</td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
  </tr>
  <tr>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">0</td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
  </tr>
  <tr>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
  </tr>
</table>


??? note solution "Réponse"

    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    </style>
    <table class="tg">
      <tr style="background-color:#ccc;">
        <th class="tg-uys7">x<br></th>
        <th class="tg-baqh">y</th>
        <th class="tg-baqh">x + y</th>
        <th class="tg-baqh">chiffre des "deuzaines"</th>
        <th class="tg-baqh">chiffre des unités</th>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-uys7">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">10</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>    


    
## Expression logique de la somme de deux bits

A l'aide du tableau d'addition de la question précédente, pour a et b deux bits, donner une expression logique à l'aide
des opérateurs and, or, xor, not du chiffre des unités de a + b et du chiffre des deuzaines de a + b.


??? note solution "Réponse"

    Avec le tableau de la question précédente, on constate que:
    
    + unite(a + b) = a xor b
    + chiffre_des_deuzaines(a + b) = a and b
    
    
    
## Application

Dans un ordinateur, des circuits (constitués notamment  de transistors) permettent d'effectuer tous les calculs
que l'on demande à l'ordinateur.

Ces circuits comportent des "portes logiques" (or, and, nor, nand, xor...) et en agençant ces portes
logiques, on peut effectuer des opérations. Nous venons de voir ci-dessus par exemple comment additionner deux bits.

Nous reviendrons plus en détails sur ce paragraphe, mais vous pouvez retenir que c'est ce qui fait l'importance
du binaire: in fine, dans l'ordinateur, toute "action" de logiciel est traduite en opérations binaires.



## Somme de trois bits


!!! note 
    Lorsqu'on effectue une addition de deux nombres entiers, on additionne en général, non pas deux bits mais trois bits.
    Il y a en effet des retenues à additionner avec les bits suivants.

Poser à la main l'addition des deux nombres binaires dans les cas ci-dessous. 
Utiliser le principe des retenues comme vous l'avez appris 
pour additionner des nombres écrits en base 10.

+ 110 + 101
+ 10111 +  101

??? note solution "110 + 101"

    On part de:
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    </style>
    <table class="tg">
      <tr style="color:orange;">
        <th class="tg-uys7">retenues</th>
        <th class="tg-baqh"> </th>
        <th class="tg-baqh"> </th>
        <th class="tg-baqh"> </th>
      </tr>
      <tr>
        <td class="tg-uys7">premier terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-uys7">second terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">résultat<br></td>
        <td class="tg-baqh"> </td>
        <td class="tg-baqh"> </td>
        <td class="tg-baqh"> </td>
      </tr>
    </table>
    
    On additionne les unités.  
    Il n'y a ici pas de retenue (en d'autres termes: le bit de retenue vaut 0): 
    
     
    <table class="tg">
      <tr  style="color:orange;">
        <th class="tg-uys7">retenues</th>
        <th class="tg-baqh"> </th>
        <th class="tg-baqh">0</th>
        <th class="tg-baqh"> </th>
      </tr>
      <tr>
        <td class="tg-uys7">premier terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-uys7">second terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">résultat<br></td>
        <td class="tg-baqh"> </td>
        <td class="tg-baqh"> </td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>
    
    
    On additionne les trois bits de la seconde colonne, cela ne génére pas non plus de retenue: 
    
     
    <table class="tg">
      <tr  style="color:orange;">
        <th class="tg-uys7">retenues</th>
        <th class="tg-baqh">0</th>
        <th class="tg-baqh">0</th>
        <th class="tg-baqh"> </th>
      </tr>
      <tr>
        <td class="tg-uys7">premier terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-uys7">second terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">résultat<br></td>
        <td class="tg-baqh"> </td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>
    
    
    On additionne   les trois bits de la colonne de gauche. On écrit directement 
    le résultat 10 ou on crée une colonne à gauche supplémentaire avec la retenue puis on additionne les 
    bits de cette nouvelle colonne:
    
        
    <table class="tg">
      <tr  style="color:orange;">
        <th class="tg-uys7">retenues</th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh">0</th>
        <th class="tg-baqh">0</th>
        <th class="tg-baqh"> </th>
      </tr>
      <tr>
        <td class="tg-uys7">premier terme<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-uys7">second terme<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">résultat<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
    </table>
    
    On a donc 110 + 101 = 1011 (soit, en décimal: 6 + 5 = 11).
    
    
??? note solution "10111 + 101"

    On commence de même:
    
  
    <table class="tg">
      <tr  style="color:orange;">
        <th class="tg-uys7">retenues</th>
        <th class="tg-baqh"></th>
        <th class="tg-baqh"></th>
        <th class="tg-baqh"></th>
        <th class="tg-baqh"></th>
        <th class="tg-baqh"> </th>
      </tr>
      <tr>
        <td class="tg-uys7">premier terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-uys7">second terme<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">résultat<br></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh"></td>
      </tr>
    </table>
    
    On ajoute les bits de la colonne des unités, ce qui génère une retenue:
    
    
    <table class="tg">
      <tr  style="color:orange;">
        <th class="tg-uys7">retenues</th>
        <th class="tg-baqh"></th>
        <th class="tg-baqh"></th>
        <th class="tg-baqh"></th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh"> </th>
      </tr>
      <tr>
        <td class="tg-uys7">premier terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-uys7">second terme<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">résultat<br></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>
    
    On ajoute alors les trois bits de la seconde colonne (en partant de la droite, colonne des deuzaines)
    sans oublier de poser la retenue générée:
    
    
 
    <table class="tg">
      <tr  style="color:orange;">
        <th class="tg-uys7">retenues</th>
        <th class="tg-baqh"></th>
        <th class="tg-baqh"></th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh"> </th>
      </tr>
      <tr>
        <td class="tg-uys7">premier terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-uys7">second terme<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">résultat<br></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>
    
    
    On poursuit ainsi:
    
    
    <table class="tg">
      <tr  style="color:orange;">
        <th class="tg-uys7">retenues</th>
        <th class="tg-baqh"></th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh"> </th>
      </tr>
      <tr>
        <td class="tg-uys7">premier terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-uys7">second terme<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">résultat<br></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>
    
    
    puis
    
   
    <table class="tg">
      <tr  style="color:orange;">
        <th class="tg-uys7">retenues</th>
        <th class="tg-baqh">0</th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh"> </th>
      </tr>
      <tr>
        <td class="tg-uys7">premier terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-uys7">second terme<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">résultat<br></td>
        <td class="tg-baqh"></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>
    
    et enfin:
    
     
    <table class="tg">
      <tr  style="color:orange;">
        <th class="tg-uys7">retenues</th>
        <th class="tg-baqh">0</th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh">1</th>
        <th class="tg-baqh"> </th>
      </tr>
      <tr>
        <td class="tg-uys7">premier terme<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-uys7">second terme<br></td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-baqh">résultat<br></td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>
    
    
    On a donc:
    
    10111 + 101 = 11100 (en décimal: 23 + 5 = 28).
    
    
## Les expressions logiques associées

Nous avons donné l'expression logique du chiffre des unités
de la somme de deux bits et du chiffre des deuzaines de deux bits.

Mais nous venons de voir, qu'avec les retenues, c'est l'expression du chiffre des unités et du chiffre des deuzaines
de la somme de **trois** bits qui est nécessaire pour construire un circuit calculant une somme.


Ces expressions sont plus complexes qu'avec deux bits, les voici:

+ unite(a + b + c) = (a et non(b) et non(c)) ou (non(a) et b et non(c)) ou (non(a) et non(b) et c) ou (a et b et c)
+ deuzaine(a + b + c) = (a et b) ou (b et c) ou (a et c)

### Exercice (facultatif)

Ecrire des fonctions python qui vérifieront que ces deux formules sont correctes.

!!! note "Aide technique"

    On pourra éventuellement utiliser `int` et `bool`.
    
    Dans un interpréteur python:
    
    ```
    >>> int(False), int(True)
    (0, 1)
    >>> bool(0), bool(1)
    (False, True)
    ```
    
    Mais vous n'en aurez pas nécessairement besoin:
    
    ```
    >>> False + False
    0
    >>> False + True
    1
    >>> True + True
    2
    ```



??? note solution "Une réponse"

    Voici un exemple de code. Travaillez le, testez le...
    
    ```python
    def unite(a,b,c):
        t1 = a and not(b) and not(c)
        t2 = not(a) and b and not(c)
        t3 = not(a) and not(b) and c
        t4 = a and b and c
        return t1 or t2 or t3 or t4
        
        
    def deuzaine(a, b, c):
        t1 = a and b
        t2 = b and c
        t3 = c and a
        return t1 or t2 or t3
        
        
        
    def verification():
        for a in (0, 1):
            for b in (0, 1):
                for c in (0, 1):
                    s  = a + b + c
                    if s%2 != unite(a,b,c) or s//2 != deuzaine(a,b,c):
                        return False
        return True
        
        
    print(verification())
    ```
    
    
## Partie optionnelle

Vous travaillerez les pages de [ce fichier](ressources/additionBinaire.pdf)
(extrait de [ce livre](https://wiki.inria.fr/wikis/sciencinfolycee/images/7/73/Informatique_et_Sciences_du_Num%C3%A9rique_-_Sp%C3%A9cialit%C3%A9_ISN_en_Terminale_S.pdf)).

