# Court-circuit



## Exercice 1

Pour évaluer "a et b" (où a et b sont deux booléens), 
si je sais que a = False, je n'ai pas besoin de connaître la valeur de b.
Expliquer pourquoi.


??? note solution "Réponse"

    On rappelle la table du and:
    
        
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    </style>
    <table class="tg">
      <tr style="background:#999;">
        <th class="tg-uys7">a<br></th>
        <th class="tg-uys7">b<br></th>
        <th class="tg-c3ow">a et b<br></th>
      </tr>
      <tr style="background:red;">
        <td class="tg-uys7">faux</td>
        <td class="tg-uys7">faux</td>
        <td class="tg-c3ow">faux</td>
      </tr>
      <tr style="background:red;">
        <td class="tg-uys7">faux</td>
        <td class="tg-uys7">vrai</td>
        <td class="tg-c3ow">faux</td>
      </tr>
      <tr>
        <td class="tg-c3ow">vrai</td>
        <td class="tg-c3ow">faux</td>
        <td class="tg-c3ow">faux</td>
      </tr>
      <tr>
        <td class="tg-c3ow">vrai</td>
        <td class="tg-c3ow">vrai</td>
        <td class="tg-c3ow">vrai</td>
      </tr>
    </table>     
    
    Lorsque a = False, on  a  "a et b = False", quelque soit la valeur de b.

## Exercice 2

Existe-il de même une valeur de a pour laquelle je n'ai pas besoin de celle de b 
pour connaître la valeur de "a ou b"?


??? note solution "Réponse"

    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
    .tg .tg-uys7{border-color:inherit;text-align:center}
    </style>
    <table class="tg">
      <tr style="background:#999;">
        <th class="tg-uys7">a<br></th>
        <th class="tg-uys7">b<br></th>
        <th class="tg-c3ow">a ou b<br></th>
      </tr>
      <tr>
        <td class="tg-uys7">faux</td>
        <td class="tg-uys7">faux</td>
        <td class="tg-c3ow">faux</td>
      </tr>
      <tr>
        <td class="tg-uys7">faux</td>
        <td class="tg-uys7">vrai</td>
        <td class="tg-c3ow">vrai<br></td>
      </tr>
      <tr style="background:red;">
        <td class="tg-c3ow">vrai</td>
        <td class="tg-c3ow">faux</td>
        <td class="tg-c3ow">vrai</td>
      </tr>
      <tr style="background:red;">
        <td class="tg-c3ow">vrai</td>
        <td class="tg-c3ow">vrai</td>
        <td class="tg-c3ow">vrai</td>
      </tr>
    </table>

    On voit qu'avec a = False, la réponse dépend de la valeur de b.
    Par contre avec a = True, on aura "a ou b = True" indépendamment de la valeur de b.
    
    Donc lorsque je sais que a = True, 
    je n'ai pas besoin de connaître la valeur de b pour connaître la valeur de (a ou b).
    
    
##  Court-circuit

Le langage Python (ainsi que d'autres langages) profite de la remarque précédente pour évaluer une expression "a et b"
 ou une expression "a ou b".
 
Par exemple, pour une expression "a et b", si a vaut False, b n'est pas évalué. 

!!! note
    La [documentation python](https://docs.python.org/fr/3/library/stdtypes.html?highlight=short%20circuit%20operators#boolean-operations-and-or-not) décrit cette évaluation
    en parlant de [court-circuit](https://en.wikipedia.org/wiki/Short-circuit_evaluation).


## Exercice 3


+ Qu'obtient-on avec l'entrée suivante dans un interpréteur python?

```
>>> ch = ''
>>> ch[0] == 'a'
```

??? note solution "Réponse"
    On obtient l'affichage d'une erreur bien sûr: `IndexError: string index out of range`.
    
 
+ Qu'obtient-on avec l'entrée suivante dans un interpréteur python? Expliquer.

```
>>> ch = ''
>>> if len(ch) != 0 and ch[0] == 'a':
...     print(ch)
... else:
...     print("Bouh!")
```

??? note solution "Réponse"
    On obtient l'affichage `Bouh!`.
    Cette réponse peut paraître étrange: pourquoi n'obtient-on pas une erreur? 
    
    Comme (len(ch) != 0) = False, une expression logique " (len(ch) != 0 ) et ..." vaudra nécessairement False.
    Python n'évalue donc pas ce qui se trouve après le "and" : l'erreur IndexError n'a donc pas lieu.
    
    
## Exercice 4

Hugo voudrait afficher la valeur de x lorsque x vaut 0 ou lorsque x vaut 1, 
et afficher "Erreur" lorsque x ne vaut ni 0, ni 1.

Il écrit le code suivant:

```python
if x == 1 or 0:
    print(x)
else:
    print("Erreur.")
```

+ Ce code convient-il? 

??? solution "Réponse"

    Ce code ne convient pas, comme le montre le test:
    
        
    ```python
    x = 0
    if x == 1 or 0:
        print(x)
    else:
        print("Erreur.")
    ```
        
    qui affiche "Erreur." alors que x a bien été initialisé à 0.
    
    
    
    
+ Expliquez l'erreur faite par Hugo.


??? solution "Réponse"

    Pour mieux comprendre ce qu'a écrit Hugo, ajoutons des parenthèses:
    
    ```python
    if (x == 1) or 0:
    ```
    
    Le or, au lieu de porter sur deux booléens, porte d'un côté sur le booléen `x == 1` et de l'autre sur 
    le nombre 0. Cela n'a, d'un point de vue logique, aucun sens!
    
    Il faut écrire:
    
    ```python
    if (x == 1) or (x == 0):
    ```
    
    Un `or` est un opérateur **entre deux booléens**.
    
??? solution "Remarque" 

    Il est légitime de se demander pourquoi Python n'affiche pas une erreur.  
    Des éléments de réponse dans le code:
    
    ```
    >>> 1 == 1 or 0
    True
    >>> 1 == 0 or 0
    0
    >>> bool(0)
    False
    >>> 'a' == 'b' or 'c'
    'c'
    ```
    
    Python donne à `or` une signification qui n'est pas uniquement celle de la logique. 
    Il **vous est interdit** en NSI d'utiliser `or` autrement qu'entre deux booléens. **Toute utilisation autre
    sera considérée par le correcteur comme une erreur.**
    
    
    Vous pourrez vérifier sur quelques exemples que `t = a or b`
    a la même valeur que `t = a if a else b`.
    
    De même, `t = a and b` aura la valeur de `t = a if not(a) else b`.
    

    
    

 
 
